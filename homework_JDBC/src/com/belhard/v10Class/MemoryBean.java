package com.belhard.v10Class;

public class MemoryBean {
	private String basketOfGoods = "";

	public String getBasketOfGoods() {
		return basketOfGoods;
	}

	public void setBasketOfGoods(String basketOfGoods) {
		this.basketOfGoods = basketOfGoods;
	}

}