package com.belhard.v10Class;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class v10ClassServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		// response.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		// приём из сессии
		MemoryBean memory10 = (MemoryBean) session.getAttribute("memory10");
		if (memory10 == null) memory10 = new MemoryBean();

		// приём параметров
		String inputProductName = request.getParameter("productName");
		String inputDeleteProduct = request.getParameter("deleteProduct");

		// тут бизнес-код. добавляем товар в корзину
		if (inputProductName != null) {
			// добавить продукт в БД: подключиться, записать значение, закрыть
			MyDB connect = new MyDB();
			Connection con = connect.loadAndConnect();
			Statement stat = null;
			ResultSet result = null;

			try {
				stat = con.createStatement();
				stat.execute("USE TRAINING_DB");
	stat.execute("INSERT INTO PRODUCTS (PRODUCTS_NAME) VALUES ('" + inputProductName + "')");
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		// удаление товара с корзины
		if (inputDeleteProduct != null) {
			MyDB connect2 = new MyDB();
			Connection con2 = connect2.loadAndConnect();
			Statement stat2 = null;
			
			try {
				
				stat2 = con2.createStatement();
				stat2.execute("USE TRAINING_DB");
	stat2.executeUpdate("DELETE FROM PRODUCTS WHERE PRODUCTS_NAME = '"+inputDeleteProduct+"';");// WHERE Message=" + inputDeleteProduct+";"
	con2.close();stat2 = null;con2=null; connect2=null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		//идёт прорисовка корзины на странице
				MyDB connect1 = new MyDB();
				Connection con1 = connect1.loadAndConnect();
				Statement stat1 = null;
				ResultSet result1 = null;

				try {
					stat1 = con1.createStatement();
					stat1.execute("USE TRAINING_DB");
					// берём значения из бд
					result1 = stat1.executeQuery("SELECT PRODUCTS_NAME FROM PRODUCTS");
					String str1 = "";
					while (result1.next()) {
						String productName = result1.getString(1);
						str1 = str1
								+ "<button type='submit' name='deleteProduct' value="
								+ productName + ">удалить</button>" + productName
								+ "<br>";
					}
					memory10.setBasketOfGoods(str1);

					con1.close(); result1 = null;stat1 = null;con1=null; connect1=null;
				} catch (SQLException e) {
					e.printStackTrace();
				}

		// запаковали сессию
		session.setAttribute("memory10", memory10);

		// формирование ответа
		RequestDispatcher dispatcher = request
				.getRequestDispatcher("/WEB-INF/pages/zadanie_10_MVC.jsp");
		dispatcher.forward(request, response);
	}
}
