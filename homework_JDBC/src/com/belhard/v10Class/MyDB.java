package com.belhard.v10Class;

import java.sql.*;

public class MyDB {

	//1.Подключить JDBC драйвер для СУБД.
	//2.Соединиться с Базой Данных.
	//3.Сформировать и выполнить запрос.
	//4.Обработать результаты запроса.
	//5.Закрыть ранее открытые ресурсы.

	public static Connection loadAndConnect() {
		Connection connection = null;
		//load driver
		try {
			//1.загрузке класса драйвера и инициализации его статической части.
			//JDBC драйвер при статической инициализации регистрирует себя в DriverManager'е.
			Class.forName("com.mysql.jdbc.Driver");
			//2.Соединение с БД
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		//connect
		try {
			connection = DriverManager.getConnection("jdbc:mysql://mysql-env-9035846.jelastic.dogado.eu/TRAINING_DB", "root", "H99wVSfD9T");
			return connection;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}	
}			

/*			
			//выбор таблицы
			statement = connection.prepareStatement("SELECT * FROM PRODUCTS");
			//полцчение объекта с таблицей
			resultSet = statement.executeQuery();
			//последовательное построчное чтение данных из таблицы
			while (resultSet.next()) {
				//колонка id и название продукта
				Integer productId = resultSet.getInt("PRODUCTS_ID");
				String productName = resultSet.getString("PRODUCTS_NAME");
				
				System.out.println(productId + " - " + productName);
			}
		} catch (Exception e) {
			/*было ServletException, сенсей порекомендовал РайнтаймЭксепшен*/
/*			throw new RuntimeException(e);
		} finally {
			if (resultSet!=null) {
				try {
					resultSet.close();
					resultSet=null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if (statement!=null) {
				try {
					statement.close();
					statement=null;
				} catch (SQLException e) {
				}
			}
			
			if (connection!=null) {
				try {
					connection.close();
					connection=null;
				} catch (SQLException e) {
					
				}
			}
		}
	}

}
*/